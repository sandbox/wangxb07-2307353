/**
 * @file
 * javascript for glide pane
 */
(function($) {

  Drupal.behaviors.pluginExtSlideshow = {
    attach: function (context, settings) {
      var targetId = settings.glideSlideshow.targetId;

      $('#' + targetId).glide({
        arrows: settings.glideSlideshow.arrows,
        navigationCenter: settings.glideSlideshow.navigationCenter
      });
    }
  };

})(jQuery);
