<?php
/**
 * @file
 * theme template for glide pane
 * @variables
 *   $conf the config for pane
 */
?>

<div class="slider" id="slider-<?php echo $id?>">
  <section class="slides">
    <?php for ($i = 0; $i < variable_get('glide_pane_content_type_count', 4); $i ++): ?>
      <?php if (!empty($conf['url_' . $i])): ?>
        <article class="slide"><img src="<?php echo $conf['url_' . $i];?>" /></article>
      <?php endif ?>
    <?php endfor ?>
  </section>
</div>
