<?php

$plugin = array(
  'title' => t('Glide content type'),
  'content_types' => 'Others',
  'single' => TRUE,
  'render callback' => 'glide_pane_content_type_render',
  'description' => t('The content type implements slideshow by glide library.'),
  'edit form' => 'glide_pane_content_type_edit_form',
  'category' => 'Custom'
);

/**
 * Run-time rendering of the body of the block (content type)
 * @param $subtype
 * @param $conf
 * Configuration as done at admin time
 * @param $args
 * @param $context
 * Context - in this case we don't have any
 *
 * @return
 * An object with at least title and content members
 */
function glide_pane_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $block->title = '';
  $block->content = theme('glide_slideshow', array('conf' => $conf));

  return $block;
}

/**
 * 'Edit' callback for this content type.
 */
function glide_pane_content_type_edit_form($form, $form_state) {
  $length = variable_get('glide_pane_content_type_count', 4);

  $conf = $form_state['conf'] + array(
    'arrows' => FALSE,
    'navigation_center' => TRUE,
  );

  $form['arrows'] = array(
    '#type' => 'checkbox',
    '#title' => t('Slide Arrows'),
    '#default_value' => $conf['arrows'],
    '#description' => t('Enable or disable the slideshow arrows'),
  );
  $form['navigation_center'] = array(
    '#type' => 'checkbox',
    '#title' => t('Navigation center'),
    '#default_value' => $conf['navigation_center'],
    '#description' => t('Center bullet navigation'),
  );

  for ($i = 0; $i < $length; $i++) {
    if (!isset($conf['url_' . $i])) {
      $conf['url_' . $i] = '';
    }

    $form['url_' . $i] = array(
      '#type' => 'textfield',
      '#title' => t('Slide url @i', array('@i' => $i)),
      '#default_value' => $conf['url_' . $i],
    );
  }

  return $form;
}

/**
 * submit callback for this content type edit form.
 */
function glide_pane_content_type_edit_form_submit($form, $form_state) {
  foreach (element_children($form) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}
